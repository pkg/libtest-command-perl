libtest-command-perl (0.11-4+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 19:55:31 +0000

libtest-command-perl (0.11-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtest-command-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 18:35:39 +0100

libtest-command-perl (0.11-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:57:07 +0100

libtest-command-perl (0.11-2.1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 20:42:45 +0000

libtest-command-perl (0.11-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 29 Dec 2020 02:26:41 +0100

libtest-command-perl (0.11-2co1) apertis; urgency=medium

  * debian/apertis/component: Set to development
  * debian/apertis/gitlab-ci.yml: Drop since we use an external definition

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 01 Mar 2021 08:11:38 +0000

libtest-command-perl (0.11-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Jun 2015 17:43:03 +0200

libtest-command-perl (0.11-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 0.11.
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0, directly
    link GPL-1+).
  * Bump dh compatibility to level 8 (no changes necessary).
  * Switch to short-form debian/rules.
  * Switch to source format 3.0 (quilt).
  * Add myself to uploaders and copyright.

 -- Florian Schlichting <fsfs@debian.org>  Sun, 11 Aug 2013 22:50:30 +0200

libtest-command-perl (0.08-1) unstable; urgency=low

  * New upstream release
  * debian/control: added myself to Uploaders
  * debian/watch: remove comment
  * debian/copyright: update copyright for debian/*

 -- Brian Cassidy <brian.cassidy@gmail.com>  Thu, 14 May 2009 09:30:49 -0300

libtest-command-perl (0.04-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ansgar Burchardt ]
  * New upstream release.
  * Add myself to Uploaders.
  * Refresh debian/rules for debhelper 7.
  * Add perl-modules (>= 5.10) as an alternative build-dep to
    libmodule-build-perl.

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 11 May 2009 20:19:16 +0200

libtest-command-perl (0.02-1) unstable; urgency=low

  [ Ernesto Hernández-Novich (USB) ]
  * Initial Release (Closes: #460100).

 -- Damyan Ivanov <dmn@debian.org>  Tue, 15 Jan 2008 21:24:17 +0200
